<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use InvalidArgumentException;
use Laravel\Prompts\Prompt;

class IDPController extends Controller
{
    public function login(Request $request)
    {
        
        $request->session()->put("state", $state =  Str::random(40));
        $query = http_build_query([
            "client_id" => env('IDP_CLIENT_ID'),
            "redirect_uri" => route('idp.callback'),
            "response_type" => "code",
            "scope" => "",
            "state" => $state
        ]);
        return redirect(env('IDP_URL') .  "/oauth/authorize?" . $query);
    }
    public function callback(Request $request)
    {
        $state = $request->session()->pull("state");

        if(!empty($state))

        if(empty($state) || $state != $request->state){
            throw new InvalidArgumentException;            
        }

        $response = Http::asForm()->post(
            env('IDP_URL') .  "/oauth/token",
            [
                "grant_type" => "authorization_code",
                "client_id" => env('IDP_CLIENT_ID'),
                "client_secret" => env('IDP_CLIENT_SECRET'),
                "redirect_uri" => route("idp.callback") ,
                "code" => $request->code
            ]
        );
        
        $request->session()->put($response->json());
        
        return redirect(route("idp.connect"));
    }
    public function connect(Request $request)
    {
        $access_token = $request->session()->get("access_token");
        $response = Http::withHeaders([
            "Accept" => "application/json",
            "Authorization" => "Bearer " . $access_token
        ])->get(env('IDP_URL') .  "/api/user");
        $userArray = $response->json();
        
        try {
            $email = $userArray['email'];
        } catch (\Throwable $th) {
            return redirect("login")->withError("Failed to get login information! Try again.");
        }
        $user = User::where("email", $email)->first();
        if (!$user) {
            $user = new User;
            $user->name = $userArray['name'];
            $user->email = $userArray['email'];
            $user->email_verified_at = $userArray['email_verified_at'];
            $user->save();
        }
        Auth::login($user);
        return redirect(route("welcome"));
    }
}